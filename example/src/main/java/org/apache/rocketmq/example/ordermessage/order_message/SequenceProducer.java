package org.apache.rocketmq.example.ordermessage.order_message;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.io.UnsupportedEncodingException;
import java.util.*;

/***********************
 * 根据选择器投放至指定的队列
 * @author MrFox
 * @date 2020/7/8 23:17
 * @version 1.0
 * @description
 ************************/
public class SequenceProducer {

   static class Node{
       private Node next;
       private String action;
       private String orderId;
       private Integer userId;

       Node(Node next, String action, String orderId,Integer userId) {
           this.next = next;
           this.action = action;
           this.orderId = orderId;
           this.userId = userId;
       }

       @Override
       public String toString() {
           return "Node{" +
                   "next=" + next +
                   ", action='" + action + '\'' +
                   ", orderId='" + orderId + '\'' +
                   '}';
       }
   }

    public static void main(String[] args) {
        try {

            List<Node> nodeList = buildOrder();

            DefaultMQProducer producer = new DefaultMQProducer("order_group_id");
            producer.setNamesrvAddr("127.0.0.1:9876");
            producer.start();

            while (!nodeList.isEmpty()){
                Random random = new Random();
                int nextInt = random.nextInt(nodeList.size());

                Node node = nodeList.get(nextInt);
                System.out.println(node.orderId+node.action);



                Message msg =
                        new Message("order_topic", "*",
                                (node.orderId + node.action ).getBytes(RemotingHelper.DEFAULT_CHARSET));
                SendResult sendResult = producer.send(msg, (mqs, msg1, arg) -> {
                    Integer id = (Integer) arg;
                    int index = id % mqs.size();
                    return mqs.get(index);
                }, node.userId);

                System.out.printf("%s%n", sendResult);

                //去除当前node
                if(Objects.nonNull(node.next)){
                    nodeList.set(nextInt, node.next) ;
                }else {
                    nodeList.remove(nextInt);
                }
            }

            producer.shutdown();
        } catch (MQClientException | RemotingException | MQBrokerException | InterruptedException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    /**********************
     * 生成100个订单
     * @param
     * @return
     * @description //TODO
     * @date 23:28 2020/7/8
    **********************/
     public static List<Node> buildOrder(){
         Random random = new Random();
        List<Node> nodeList = new ArrayList<>();
        //for (int x= 0;x<100;x++){
         for (int x= 0;x<1;x++){
            int userId = random.nextInt(10);
            UUID uuid = UUID.randomUUID();
            String toString = uuid.toString();
            Node placeOrder = new Node(
                    new Node(new Node(null,"-物流订单", toString,userId),"-付款订单",toString,userId),"-下单订单", toString,userId);
            nodeList.add(placeOrder);
        }
        return nodeList;
     }


}
